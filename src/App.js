//import '../src/App.css';
import React from "react";
import TodoList from './components/ToDoList';
import AppHeader from './components/AppHeader';
import SearchBar from './components/SearchBar';
import StatusFilter from "./components/StatusFilter";
import AddItem from "./components/AddItem";
import './index.css';


class App extends React.Component {

    maxId = 200;

    state =  {
        todoData : [
            {label: 'Drink!', isImportant: true, done: true, id: 1},
            { label: 'Learn React', isImportant: true, done: false, id: 2 },
            { label: 'Drink again!', isImportant: false, done: true, id: 3 },
            { label: 'Build React App', isImportant: false, done: true, id: 4 },
            {label: 'Build again!', isImportant: true, done: false, id: 5}
        ],
        search: '',
        filter: 'all'
    };
  //const todos = ['Drink!', 'Learn', 'Build', 'Build again'];
      deleteItem = (id) => {
        this.setState(({ todoData }) => {
            const idx = todoData.findIndex((el) => el.id === id);
            const beforeArray = todoData.slice(0, idx);
            const afterArray = todoData.slice(idx+1);
            const newArray = [ ...beforeArray, ...afterArray ];

            return {
                todoData: newArray
            }
        });
    }

    onToggleProperty = (arr, id, propName) => {
        const idx = arr.findIndex((el) => el.id === id);
        const oldItem = arr[idx];
        const newItem = { ...oldItem, [propName]: !oldItem[propName]};

        const newArr = [
            ...arr.slice(0, idx),
            newItem,
            ...arr.slice(idx + 1)
        ];

        return {
            todoData: newArr
        }
    }

    onToggleImportant = (id) => {
        this.setState(({todoData}) => {
            const idx = todoData.findIndex((el) => el.id === id);
            const oldItem = todoData[idx];
            const newItem = { ...oldItem, isImportant: !oldItem['isImportant']};

            const newArr = [
                ...todoData.slice(0, idx),
                newItem,
                ...todoData.slice(idx + 1)
            ];

            return {
                todoData: newArr
            }
        });
    }

    onToggleDone = (id) => {
        this.setState(({todoData}) => this.onToggleProperty(todoData, id, 'done'));
    }

    onFilterChange = (name) => {
        this.setState({
            filter: name
        });
    }

    onSearchChange = (search) => {
        this.setState({
            search: search
        });
    }

    addItem = (text) => {
        const newItem = {
            label: text,
            isImportant: false,
            done: false,
            id: this.maxId++
        };

        this.setState(({todoData}) => {
            const newArr = [
                ...todoData,
                newItem
            ];

            return {
                todoData: newArr
            }
        });
    }
      render() {

        const { todoData, filter, search } = this.state;
        console.log('search', search);
        const doneCount = this.state.todoData.filter((el) => el.done).length;
        const todoCount = this.state.todoData.length - doneCount;

        const filtered = todoData.filter((item) => {
            if (filter === 'all') {
                return true;
            }

            if (filter === 'active') {
                return !item.done;
            }

            if (filter === 'done') {
                return item.done;
            }
        });

        const searchFiltered = filtered.filter((item) => item.label.toUpperCase().indexOf(this.state.search.toUpperCase()) != -1)
  return (
    <div className="App">
      {/* {<header className="App-header" >
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Hi! Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>} */}
      {/* {<div><p>Hello, world!!!</p></div>} */}


      {<AppHeader toDo={todoCount} done={doneCount} />}
      {<SearchBar onSearchChange={ this.onSearchChange} />}
      <StatusFilter filter={filter} onFilterChange={this.onFilterChange} />
      {<TodoList todos={searchFiltered}
        onDelete={this.deleteItem}
        onToggleImportant={this.onToggleImportant}
        onToggleDone={this.onToggleDone}
      />}
      <AddItem onItemAdded={this.addItem} />
    </div>
  );
}
}

export default App;
